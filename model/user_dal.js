var mysql   = require('mysql');
var db  = require('../model/db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view user_view as
 select s.*, a.state, a.country from user s
 join location a on a.location_id = s.location_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM user;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(user_id, callback) {
    var query = 'SELECT c.*, a.state, a.country FROM user c ' +
        'LEFT JOIN user_location ca on ca.user_id = c.user_id ' +
        'LEFT JOIN location a on a.location_id = ca.location_id ' +
        'WHERE c.user_id = ?';
    var queryData = [user_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO user (user_name) VALUES (?)';

    var queryData = [params.user_name];

    connection.query(query, params.user_name, function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var user_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO user_location (user_id, location_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var userLocationData = [];
        if (params.location_id.constructor === Array) {
            for (var i = 0; i < params.location_id.length; i++) {
                userLocationData.push([user_id, params.location_id[i]]);
            }
        }
        else {
            userLocationData.push([user_id, params.location_id]);
        }

        // NOTE THE EXTRA [] AROUND userLocationData
        connection.query(query, [userLocationData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(user_id, callback) {
    var query = 'DELETE FROM user WHERE user_id = ?';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var userLocationInsert = function(user_id, locationIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO user_location (user_id, location_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var userLocationData = [];
    if (locationIdArray.constructor === Array) {
        for (var i = 0; i < params.location_id.length; i++) {
            userLocationData.push([user_id, params.location_id[i]]);
        }
    }
    else {
        userLocationData.push([user_id, params.location_id]);
    }
    connection.query(query, [userLocationData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.userLocationInsert = userLocationInsert;

//declare the function so it can be used locally
var userLocationDeleteAll = function(user_id, callback){
    var query = 'DELETE FROM user_location WHERE user_id = ?';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.userLocationDeleteAll = userLocationDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE user SET user_name = ? WHERE user_id = ?';
    var queryData = [params.user_name, params.user_id];

    connection.query(query, queryData, function(err, result) {
        //delete user_location entries for this user
        userLocationDeleteAll(params.user_id, function(err, result){

            if(params.location_id != null) {
                //insert user_location ids
                userLocationInsert(params.user_id, params.location_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS user_getinfo;

     DELIMITER //
     CREATE PROCEDURE user_getinfo (user_id int)
     BEGIN

     SELECT * FROM user WHERE user_id = _user_id;

     SELECT a.*, s.user_id FROM location a
     LEFT JOIN user_location s on s.location_id = a.location_id AND user_id = _user_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL user_getinfo (4);

 */

exports.edit = function(user_id, callback) {
    var query = 'CALL user_getinfo(?)';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};