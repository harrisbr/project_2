var mysql   = require('mysql');
var db  = require('../model/db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM location;';

    connection.query(query, function(err, result) {
        console.log("location_dal getAll result: " + result);

        callback(err, result);
    });
};

exports.delete = function(location_id, callback){
    var query = 'delete from location where location_id = ' + location_id;

    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(locationObj, callback){
    var id = locationObj.location_id;
    var state = locationObj.state;
    var country = locationObj.country;
    var query= "update location set state = '" + state + "', country code = " + country + " where location_id = " + id + ";";
    console.log(query);
    connection.query(query, function(err, result){
        callback(err, result);
    });
};