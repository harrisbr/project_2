var mysql   = require('mysql');
var db  = require('../model/db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM cash;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(cash_id, callback){
    var query = 'delete from cash where cash_id = ' + cash_id;

    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(cashObj, callback) {
    var id = cashObj.cash_id;
    var cash = cashObj.cash_amount;
    var query = "update cash set cash_amount = " + cash + "where cash_id = " + id;
    connection.query(query, function (err, result) {
        callback(err, result);
    })
}