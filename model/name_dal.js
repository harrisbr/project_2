var mysql   = require('mysql');
var db  = require('../model/db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM name;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(name_id, callback){
    var query = 'delete from name where name_id = ' + name_id;

    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(nameObj, callback){
    var id = nameObj.name_id;
    var first = nameObj.first_name;
    var last = nameObj.last_name;
    var query= "update name set first_name = '" + first + "', last_name = " + last + " where name_id = " + id +";";
    connection.query(query, function(err, result){
        callback(err, result);
    });
};