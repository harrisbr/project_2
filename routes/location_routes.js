var express = require('express');
var router = express.Router();
var insert = require('../model/simple');
var location_dal = require('../model/location_dal');

router.get('/all', function(req, res) {
    location_dal.getAll(function (err, response) {
        console.log(response);
        res.render("location/locationViewAll", {response: response});
    });
});

router.post('/all', function(req, res){
    console.log("post req called");
    console.log(req.body.location);
    insert.simple("location", req.body.location, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/location/all');
        }
    });
});

router.get('/delete/:id', function(req, res){
    console.log(req.params.id);
    location_dal.delete(req.params.id, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/location/all');
        }
    });
});

router.post('/edit', function(req,res){
    console.log("post req called");
    console.log(req.body.location);
    location_dal.edit(req.body.location, function(err, result){
        if(err){
            console.log("edit threw error");
            res.send(err);

        }else{
            res.redirect('/location/all');
        }
    });
});
module.exports = router;