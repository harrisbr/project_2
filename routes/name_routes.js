var express = require('express');
var router = express.Router();
var insert = require('../model/simple');
var name_dal = require('../model/name_dal');

router.get('/all', function(req, res) {
    name_dal.getAll(function (err, response) {
        console.log(response);
        res.render("name/nameViewAll", {response: response});
    });
});

router.post('/all', function(req, res){
    console.log("post req called");
    console.log(req.body.name);
    insert.simple("name", req.body.name, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/name/all');
        }
    });
});

router.get('/delete/:name', function(req, res){
    console.log(req.params.name);
    name_dal.delete(req.params.name, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/name/all');
        }
    });
});

router.post('/edit', function(req,res){
    console.log("post req called");
    console.log(req.body.name);
    name_dal.edit(req.body.name, function(err, result){
        if(err){
            console.log("Query" + query);
            console.log("edit threw error");

        }else{
            res.redirect('/name/all');
        }
    });
});
module.exports = router;