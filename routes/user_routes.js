var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');
var location_dal = require('../model/location_dal');


// View All users
router.get('/all', function(req, res) {
    user_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('user/userViewAll', { 'result':result });
        }
    });

});

// View the user for the given id
router.get('/', function(req, res){
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    else {
        user_dal.getById(req.query.user_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('user/userViewById', {'result': result});
           }
        });
    }
});

// Return the add a new user form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    location_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('user/userAdd', {'location': result});
        }
    });
});

// View the user for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.user_name == null) {
        res.send('User Name must be provided.');
    }
    else if(req.query.location_id == null) {
        res.send('At least one location must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        user_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/user/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.user_id == null) {
        res.send('A user id is required');
    }
    else {
        user_dal.edit(req.query.user_id, function(err, result){
            res.render('user/userUpdate', {user: result[0][0], location: result[1]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.user_id == null) {
       res.send('A user id is required');
   }
   else {
       user_dal.getById(req.query.user_id, function(err, user){
           location_dal.getAll(function(err, location) {
               res.render('user/userUpdate', {user: user[0], location: location});
           });
       });
   }

});

router.get('/update', function(req, res) {
    user_dal.update(req.query, function(err, result){
       res.redirect(302, '/user/all');
    });
});

// Delete a user for the given user_id
router.get('/delete', function(req, res){
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    else {
         user_dal.delete(req.query.user_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/user/all');
             }
         });
    }
});

module.exports = router;
