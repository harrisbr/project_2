var express = require('express');
var router = express.Router();
var insert = require('../model/simple');
var cash_dal = require('../model/cash_dal');

router.get('/all', function(req, res) {
    cash_dal.getAll(function (err, response) {
        console.log(response);
        res.render("cash/cashViewAll", {response: response});
    });
});

router.post('/all', function(req, res){
    console.log("post req called");
    console.log(req.body.cash);
    insert.simple("cash", req.body.cash, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/cash/all');
        }
    });
});

router.get('/delete/:id', function(req, res){
    var id = req.params.id;
    console.log(req.params.id);
    cash_dal.delete(id, function(err, result){
        if(err){
            res.send(err);
        }else{
            res.redirect('/cash/all');
        }
    });
});

// EDIT ISN'T UPDATING, Throwing error after
router.post('/edit', function(req,res){
    console.log("post req called");
    console.log(req.body.cash);
    cash_dal.edit(req.body.cash, function(err, result){
        if(err){
            console.log("edit threw error");

        }else{
            res.redirect('/cash/all');
        }
    });
});
module.exports = router;